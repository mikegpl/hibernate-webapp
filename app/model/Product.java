package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Product implements ShopEntity {

    @Id
    @GeneratedValue
    private int productId;
    private String productName;
    private int unitsInStock;
    private int categoryId;

    @ManyToOne
    private Supplier supplier;

    public Product() {
    }

    public Product(String productName, int unitsInStock, Supplier supplier) {
        this.productName = productName;
        this.unitsInStock = unitsInStock;
        this.supplier = supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }


    public synchronized boolean makeOrderIfPossible(int quantity) {
        if (unitsInStock - quantity >= 0) {
            unitsInStock -= quantity;
            return true;
        } else {
            return false;
        }
    }

    public synchronized void resupply(int quantity) {
        unitsInStock += quantity;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getProductId() {
        return productId;
    }

    public int getUnitsInStock() {
        return unitsInStock;
    }

    public String getProductName() {
        return productName;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    @Override
    public String toString() {
        return productName;
    }
}
