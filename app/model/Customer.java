package model;

import views.HtmlUtil;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Customer implements ShopEntity {
    @Id
    private String customerName;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<ProductOrder> orders;

    public Customer() {
    }

    public Customer(String customerName) {
        this.customerName = customerName;
        this.orders = new HashSet<>();
    }

    public boolean addOrder(ProductOrder order) {
        if (order.confirmOrder()) {
            orders.add(order);
            return true;
        } else {
            return false;
        }
    }

    public Set<ProductOrder> getOrders() {
        return orders;
    }

    public String getOrdersAsString() {
        return orders.stream().map(order -> Integer.toString(order.getOrderId())).collect(Collectors.joining(HtmlUtil.NewLine()));
    }

    public String getCustomerName() {
        return customerName;
    }
}
