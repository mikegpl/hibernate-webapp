package model;

import views.HtmlUtil;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Category implements ShopEntity {
    @Id
    @GeneratedValue
    private int categoryId;
    private String name;
    @OneToMany(fetch = FetchType.EAGER)
    private List<Product> products;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
        this.products = new LinkedList<>();
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getName() {
        return name;
    }

    public void addProduct(Product product) {
        products.add(product);
        product.setCategoryId(categoryId);
    }

    public String getProductsAsString() {
        return products.stream().map(Product::getProductName).collect(Collectors.joining(HtmlUtil.NewLine()));
    }
}
