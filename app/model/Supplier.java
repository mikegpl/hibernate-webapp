package model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Supplier implements ShopEntity {

    @Id
    private String companyName;
    private String street;
    private String city;

    @OneToMany(mappedBy = "supplier", fetch = FetchType.EAGER)
    private Set<Product> products = new HashSet<>();

    public Supplier() {
    }

    public Supplier(String companyName, String street, String city) {
        this.companyName = companyName;
        this.street = street;
        this.city = city;
    }

    public void addProduct(Product p) {
        products.add(p);
        p.setSupplier(this);
    }

    public String getCity() {
        return city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getStreet() {
        return street;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", getCompanyName(), getStreet(), getCity());
    }
}
