package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ProductOrder implements ShopEntity {
    @Id
    @GeneratedValue
    private int orderId;

    @ManyToOne
    private Product target;
    private int quantity;

    public ProductOrder() {
    }

    public ProductOrder(Product product, int quantity) {
        this.target = product;
        this.quantity = quantity;
    }

    public int getOrderId() {
        return orderId;
    }


    public Product getTarget() {
        return target;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean confirmOrder() {
        return target.makeOrderIfPossible(quantity);
    }
}