package controllers

import javax.inject._

import dbservice.{DatabaseConnector, ShopDao}
import model.{Category, Customer, ProductOrder, Supplier}
import play.api.data.Forms._
import play.api.data._
import play.api.mvc._

@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with play.api.i18n.I18nSupport {
  private val sessionFactory = DatabaseConnector.SessionFactory
  private val shopDao = new ShopDao(sessionFactory)

  def index = Action {
    Ok(views.html.index("Witojcie")())
  }

  def forPath(route: String) = Action {
    route match {
      case "customers" =>
        Ok(views.html.listViews.customers.render(shopDao.getMany[Customer]("from Customer").get))
      case "categories" =>
        Ok(views.html.listViews.categories.render(shopDao.getMany[Category]("from Category").get))
      case "orders" =>
        Ok(views.html.listViews.orders.render(shopDao.getMany[ProductOrder]("from ProductOrder").get))
      case "products" =>
        Ok(views.html.listViews.products.render(shopDao.getMany[model.Product]("from Product").get))
      case "suppliers" =>
        Ok(views.html.listViews.suppliers.render(shopDao.getMany[Supplier]("from Supplier").get))
    }
  }


  /**
    * Adding new customer
    */

  def newCustomerView() = Action { implicit request =>
    Ok(views.html.adderViews.addCustomer())
  }

  def newCustomerForm = Form(mapping("name" -> text)(NewCustomerRequest.apply)(NewCustomerRequest.unapply))


  def newCustomerAction() = Action { implicit request =>
    val newRequest = newCustomerForm.bindFromRequest.get
    shopDao.saveEntity(new Customer(newRequest.name))
    Ok(views.html.listViews.customers.render(shopDao.getMany[Customer]("from Customer").get))
  }

  /**
    * Adding new order
    */
  def newOrderView() = Action { implicit request =>
    Ok(views.html.adderViews.addOrder(newOrderForm, shopDao.getMany[model.Product]("from Product").get, shopDao.getMany[Customer]("from Customer").get))
  }

  def newOrderForm = Form(mapping("Product id" -> number, "Quantity" -> number, "Customer name" -> text)(NewOrderRequest.apply)(NewOrderRequest.unapply))

  def newOrderAction() = Action { implicit request =>
    val newRequest = newOrderForm.bindFromRequest.get
    val product = shopDao.getOne[model.Product](s"from Product where productId = ${newRequest.productId}").get
    val customer = shopDao.getOne[Customer](s"from Customer where customerName = '${newRequest.customerName}'").get
    val order = new ProductOrder(product, newRequest.quantity)
    if (customer.addOrder(order)) {
      shopDao.saveEntity(product)
      shopDao.saveEntity(order)
      shopDao.saveEntity(customer)
      Ok(views.html.listViews.orders.render(shopDao.getMany[ProductOrder]("from ProductOrder").get))
    }
    else {
      Ok(views.html.index(s"Too few products $product in stock")())
    }
  }

  /**
    * resupplying
    */

  def resupplyView(productId: Int) = Action { implicit request =>
    Ok(views.html.adderViews.resupply(resupplyForm, shopDao.getMany[Supplier]("from Supplier").get, productId))
  }

  def resupplyForm = Form(mapping("Quantity" -> number, "Supplier name" -> text)(ResupplyRequest.apply)(ResupplyRequest.unapply))

  def resupplyAction(productId: Int) = Action { implicit request =>
    val resupplyRequest = resupplyForm.bindFromRequest.get
    val product = shopDao.getOne[model.Product](s"from Product where productId = $productId").get
    val supplier = shopDao.getOne[Supplier](s"from Supplier where companyName = '${resupplyRequest.supplierName}'").get
    product.setSupplier(supplier)
    product.resupply(resupplyRequest.quantity)
    shopDao.saveEntity(product)
    shopDao.saveEntity(supplier)
    Ok(views.html.listViews.products.render(shopDao.getMany[model.Product]("from Product").get))
  }

  /**
    * new supplier
    */

  def newSupplierView() = Action { implicit request =>
    Ok(views.html.adderViews.addSupplier())
  }

  def newSupplierForm = Form(mapping("name" -> text, "street" -> text, "city" -> text)(NewSupplierRequest.apply)(NewSupplierRequest.unapply))

  def newSupplierAction() = Action { implicit request =>
    val supplierRequest = newSupplierForm.bindFromRequest().get
    val supplier = new Supplier(supplierRequest.name, supplierRequest.street, supplierRequest.city)
    shopDao.saveEntity(supplier)
    Ok(views.html.listViews.suppliers(shopDao.getMany[Supplier]("from Supplier").get))
  }


  def notFound(request: String) = Action {
    Ok(views.html.index(s"Page `$request` not found")())
  }
}

case class NewSupplierRequest(name: String, street: String, city: String)

case class NewCustomerRequest(name: String)

case class NewOrderRequest(productId: Int, quantity: Int, customerName: String)

case class ResupplyRequest(quantity: Int, supplierName: String)