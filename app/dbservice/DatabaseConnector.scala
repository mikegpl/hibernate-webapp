package dbservice

import org.hibernate.SessionFactory
import org.hibernate.cfg.Configuration

object DatabaseConnector {
  private val ConfigPath = "hibernate.cfg.xml"

  val SessionFactory: SessionFactory = new Configuration().configure(ConfigPath).buildSessionFactory()
}
