package dbservice

import java.util

import model._
import org.hibernate.{Session, SessionFactory}

import scala.collection.JavaConverters._

final class ShopDao(sessionFactory: SessionFactory) {

  def getOne[T](query: String): Option[T] = {
    val session = sessionFactory.openSession()
    var result = Option.empty[T]
    withTransaction(session) {
      result = Some(session.createQuery(query, classOf[ShopEntity]).getSingleResult.asInstanceOf[T])
    }
    result
  }

  def getMany[T](query: String): Option[List[T]] = {
    val session = sessionFactory.openSession()
    var result = Option.empty[java.util.List[ShopEntity]]
    withTransaction(session) {
      result = Some(session.createQuery(query, classOf[ShopEntity]).getResultList)
    }
    Option(result.getOrElse(new util.ArrayList[T]()).asScala.collect { case e: T => e }.toList)
  }

  def saveEntity(entity: ShopEntity): Unit = {
    val session = sessionFactory.openSession()
    withTransaction(session) {
      session.saveOrUpdate(entity)
    }
  }

  protected def withTransaction(session: Session)(code: => Unit): Unit = {
    val transaction = session.beginTransaction()
    code
    transaction.commit()
    session.close()
  }
}